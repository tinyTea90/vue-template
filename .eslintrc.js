/*
 * @Author: 陈铭
 * @Date: 2020-08-18 10:05:11
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-08-29 16:03:00
 * @Description: file content
 * @FilePath: /vue-template/.eslintrc.js
 */
module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint",
    sourceType: "module"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "prettier/prettier": ["error", {
      semi: false,
      trailingComma: "none",
      singleQuote: true
    }],
  }
};
