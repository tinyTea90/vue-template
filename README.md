# vue-template

vue-template是为了方便开发做得基础项目包。

其中包括`mock`、`axios`、`vuex`的基本配置，可以根据个人需要进行进一步的调整。

## 安装
```
npm install
```
通过上面的代码安装项目所需插件。

## 配置
配置项中的配置，可以根据项目需要调整。
### .env
项目的基础配置文件，其中的配置项会被应用到整个项目。

### .env.development
前端本地测试用配置，将调用`mock`的虚拟数据服务。可覆盖`.env`配置。

### .env.test
后台联调用配置，将调用后台接口数据。可覆盖`.env`配置。

### .env.production
打包用配置。可覆盖`.env`配置。

## 运行

```.shell
# 运行本地测试用服务
npm run server

# 运行后台联调测试服务
npm run server:test

# 打包项目文件
npm run build
```

## 声明
本项目包创建初始目的是为了方便个人开发而搭建好的基础项目包。希望可以给你提供帮助。

如有问题欢迎通过发送issue等方式讨论。