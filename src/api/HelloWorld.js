import request from '@/utils/request'
/*
 * @Author: 陈铭
 * @Date: 2020-06-08 09:12:48
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-06-08 11:16:48
 */
export function getHello(data) {
  return request({
    url: '/getHello',
    method: 'post',
    data
  })
}
