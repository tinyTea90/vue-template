/*
 * @Author: 陈铭
 * @Date: 2020-06-08 09:35:59
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-06-08 15:05:28
 */
export default [
  {
    url: '/getHello',
    type: 'post',
    response: req => {
      const { data } = JSON.parse(req.body)

      let msg = `wrong word: ${data}`

      if (data.toLocaleLowerCase() === 'world') {
        msg = 'hello'
      }

      return {
        data: msg
      }
    }
  }
]
