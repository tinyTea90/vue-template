import HelloWorld from './HelloWorld'

/*
 * @Author: 陈铭
 * @Date: 2020-06-08 09:37:43
 * @Last Modified by:   陈铭
 * @Last Modified time: 2020-06-08 09:37:43
 */
export default [...HelloWorld]
