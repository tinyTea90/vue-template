/*
 * @Author: 陈铭
 * @Date: 2020-06-08 15:09:12
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-12-29 11:18:31
 */
const env = process.env
const FileManagerPlugin = require('filemanager-webpack-plugin')
const nowDate = new Date()
const packageConfig = require('./package.json')

const config = {
  publicPath: env.VUE_APP_PUBLIC_PATH,
  outputDir: env.VUE_APP_OUTPUT_DIR,
  assetsDir: env.VUE_APP_ASSETS_DIR,
  lintOnSave: env.NODE_ENV === 'development' ? 'error' : false,
  productionSourceMap: false,
  devServer: {
    port: env.VUE_APP_PORT
  },
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = env.VUE_APP_PAGE_TITLE
      return args
    })
  },
  configureWebpack: {
    devtool: 'source-map'
  }
}

if (env.NODE_ENV === 'backserver') {
  config['devServer'] = {
    ...config.devServer,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      [env.VUE_APP_BASE_URL]: {
        target: `http://${env.VUE_APP_BACKSTAGE_IP}:${env.VUE_APP_BACKSTAGE_PORT}${env.VUE_APP_BASE_URL}`,
        changeOrigin: true,
        pathRewrite: {
          [`^${env.VUE_APP_BASE_URL}`]: ''
        }
      }
    }
  }
}

if (env.NODE_ENV === 'production') {
  config['configureWebpack'] = {
    plugins: [
      new FileManagerPlugin({
        events: {
          onEnd: {
            delete: ['./zip'],
            archive: [
              {
                source: './dist',
                destination: `./zip/${
                  packageConfig.name
                }_${nowDate.getFullYear()}${nowDate.getMonth() +
                  1}${nowDate.getDate()}_v.${packageConfig.version}.zip`
              }
            ]
          }
        }
      })
    ]
  }
}

module.exports = config
